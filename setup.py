from pathlib import Path
from setuptools import setup, find_packages


def find_pip_requirements():
    req = Path('./docker/requirements.txt')
    return req.read_text().splitlines()


setup(
    name='dcd',
    version="0.1",
    description="Donkey Car Drifting Package",
    packages=find_packages(include=['dcd'], exclude=['tests']),
    install_requires=find_pip_requirements(),
)
