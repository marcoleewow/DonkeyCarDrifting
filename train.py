import os
import click
import numpy as np
from pathlib import Path
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint

from dcd.dataloaders import TrackMaskDataloader
from dcd.models import UNet, BiSeNet
from dcd.losses import jaccard_distance, dice_loss, custom_loss


@click.command()
@click.option('--gpu', type=int, default=0)
@click.option('--n-epochs', type=int, default=5000)
@click.option('--lr', type=float, default=3e-4)
@click.option('--batch-size', type=int, default=20)
@click.option('--overfit-batch', is_flag=True, default=False)
@click.option('--trim-data', type=int, default=0)
@click.option('--model-type', type=str, default='unet')
@click.option('--log-dir', type=str, default='./logs/')
def train(gpu, n_epochs, lr, batch_size, overfit_batch, model_type, trim_data, log_dir):
    if overfit_batch:
        trim_data = batch_size
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu)
    np.random.seed(1234)

    img_size = (120, 160, 3)

    if model_type == 'unet':
        optimizer = Adam(lr=lr)
        # loss = jaccard_distance
        # loss = binary_crossentropy
        # loss = dice_loss
        loss = custom_loss
        model = UNet(input_size=img_size, optimizer=optimizer, loss=loss)
    elif model_type == 'bisenet':
        optimizer = Adam(lr=lr)
        def categorical_crossentropy(y_true, y_pred):
            return K.categorical_crossentropy(y_true, y_pred, from_logits=True)
        loss = categorical_crossentropy
        model = BiSeNet(input_size=img_size, optimizer=optimizer, loss=loss)

    train_loader = TrackMaskDataloader(data_path="/workspace/data/track_data",
                                       dataset_type="train",
                                       csv_path="/workspace/data/labelbox.csv",
                                       trim_data=trim_data,
                                       batch_size=batch_size)

    valid_loader = TrackMaskDataloader(data_path="/workspace/data/track_data",
                                       dataset_type="valid",
                                       csv_path="/workspace/data/labelbox.csv",
                                       trim_data=trim_data,
                                       batch_size=batch_size)

    if overfit_batch:
        assert len(train_loader) == 1
        assert len(valid_loader) == 1
    log_dir = Path(log_dir)
    log_dir = log_dir / '{}_lr_{}_bs_{}'.format(model_type, lr, batch_size)
    log_dir.mkdir(exist_ok=True, parents=True)
    model_checkpoint = ModelCheckpoint(str(log_dir / 'weights.h5'), monitor='val_loss', save_best_only=True)
    model.fit_generator(train_loader,
                        epochs=n_epochs,
                        validation_data=valid_loader,
                        callbacks=[model_checkpoint])


if __name__=="__main__":
    train()
