import os
import click
import numpy as np
from pathlib import Path
import matplotlib.image as mpimg
from PIL import Image
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint

from dcd.dataloaders import TrackMaskDataloader
from dcd.models import UNet, BiSeNet
from dcd.losses import jaccard_distance


@click.command()
@click.option('--gpu', type=int, default=0)
@click.option('--model-type', type=str, default='unet')
@click.option('--log-dir', type=str, default='')
def error_analysis(gpu, model_type, log_dir):
    batch_size = 1
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu)
    np.random.seed(1234)

    log_dir = Path(log_dir)
    assert log_dir.exists(), "please input log-dir!"

    predictions_dir = log_dir / 'predictions'
    predictions_dir.mkdir(parents=True, exist_ok=True)

    img_size = (120, 160, 3)
    lr = 0.001 # doesnt really matter

    if model_type == 'unet':
        optimizer = Adam(lr=lr)
        loss = jaccard_distance
        # loss = binary_crossentropy
        model = UNet(input_size=img_size, optimizer=optimizer, loss=loss)
    elif model_type == 'bisenet':
        optimizer = Adam(lr=lr)
        def categorical_crossentropy(y_true, y_pred):
            return K.categorical_crossentropy(y_true, y_pred, from_logits=True)
        loss = categorical_crossentropy
        model = BiSeNet(input_size=img_size, optimizer=optimizer, loss=loss)

    valid_loader = TrackMaskDataloader(data_path="/workspace/data/track_data",
                                       dataset_type="valid",
                                       csv_path="/workspace/data/labelbox.csv",
                                       batch_size=batch_size)

    assert len(valid_loader) > 1
    for i, (X, Y_mask) in enumerate(valid_loader):
        pred_mask = model.predict(X, batch_size=batch_size)
        pred_mask = np.squeeze(pred_mask)
        # filename = log_dir / '{}_prediction.npy'.format(i*batch_size)
        # np.save(filename, pred_mask)
        jpg_filename = predictions_dir / '{}_prediction.jpg'.format(i*batch_size)
        pred_mask[pred_mask > 0.1] = 1
        pred_mask[pred_mask <= 0.1] = 0
        pred_mask *= 255
        image = Image.fromarray(pred_mask)
        image = image.convert("L")
        image.save(jpg_filename)


if __name__=="__main__":
    error_analysis()
