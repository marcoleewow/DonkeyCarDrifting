import os
import click
import numpy as np
from pathlib import Path
import matplotlib.image as mpimg
from PIL import Image
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint

from dcd.dataloaders import TrackMaskDataloader
from dcd.models import UNet, BiSeNet
from dcd.losses import jaccard_distance


@click.command()
@click.option('--gpu', type=int, default=0)
def test_and_save_dataloader(gpu):
    batch_size = 1
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu)
    np.random.seed(1234)

    valid_loader = TrackMaskDataloader(data_path="/workspace/data/track_data",
                                       dataset_type="valid",
                                       csv_path="/workspace/data/labelbox.csv",
                                       batch_size=batch_size)

    for i, (X, Y_mask) in enumerate(valid_loader):
        assert Y_mask.shape[0] == batch_size
        Y_mask = np.squeeze(Y_mask)
        # since Y_mask is [0, 1] we need to change it back to 255
        Y_mask = 255 * Y_mask
        save_dir = Path('test_dataloader')
        save_dir.mkdir(parents=True, exist_ok=True)
        filename = save_dir / '{}_prediction.npy'.format(i)
        jpg_filename = save_dir / '{}_prediction.jpg'.format(i)
        image = Image.fromarray(Y_mask)
        image = image.convert("L")
        image.save(jpg_filename)
        np.save(filename, Y_mask)


if __name__=="__main__":
    test_and_save_dataloader()
