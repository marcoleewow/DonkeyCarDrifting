import tensorflow.keras.backend as K
from .dice_loss import dice_loss


def custom_loss(y_true, y_pred, l1_coeff=1e-3):
    d_loss = dice_loss(y_true, y_pred)
    binary_loss = K.sum(K.abs(y_true - y_pred))
    return d_loss + l1_coeff * binary_loss
