from abc import ABC, abstractmethod


class Model(ABC):
    @abstractmethod
    def fit_generator(self, data_generator):
        pass


