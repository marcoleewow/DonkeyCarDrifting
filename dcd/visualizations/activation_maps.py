import numpy as np

from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.layers import Convolution2D, MaxPooling2D, Reshape, BatchNormalization
import numpy as np
from pathlib import Path

from keras.models import load_model
from keras.models import Model, load_model
from keras.layers import Input, Dense
from keras.layers import Convolution2D, MaxPooling2D, Reshape, BatchNormalization
from keras.layers import Activation, Dropout, Flatten, Dense

import matplotlib.cm as cm
import matplotlib.pyplot as plt
from vis.utils import utils
from vis.visualization import visualize_cam, visualize_saliency, visualize_activation, overlay


def default_categorical(input_dimension=(120,160,3)):

    img_in = Input(shape=(input_dimension), name='img_in')                      # First layer, input layer, Shape comes from camera.py resolution, RGB
    x = img_in
    x = Convolution2D(24, (5,5), strides=(2,2), activation='relu')(x)       # 24 features, 5 pixel x 5 pixel kernel (convolution, feauture) window, 2wx2h stride, relu activation
    x = Convolution2D(32, (5,5), strides=(2,2), activation='relu')(x)       # 32 features, 5px5p kernel window, 2wx2h stride, relu activatiion
    x = Convolution2D(64, (5,5), strides=(2,2), activation='relu')(x)       # 64 features, 5px5p kernal window, 2wx2h stride, relu
    x = Convolution2D(64, (3,3), strides=(2,2), activation='relu')(x)       # 64 features, 3px3p kernal window, 2wx2h stride, relu
    x = Convolution2D(64, (3,3), strides=(1,1), activation='relu')(x)       # 64 features, 3px3p kernal window, 1wx1h stride, relu

    # Possibly add MaxPooling (will make it less sensitive to position in image).  Camera angle fixed, so may not to be needed

    x = Flatten(name='flattened')(x)                                        # Flatten to 1D (Fully connected)
    x = Dense(100, activation='relu')(x)                                    # Classify the data into 100 features, make all negatives 0
    x = Dropout(.1)(x)                                                      # Randomly drop out (turn off) 10% of the neurons (Prevent overfitting)
    x = Dense(50, activation='relu')(x)                                     # Classify the data into 50 features, make all negatives 0
    x = Dropout(.1)(x)                                                      # Randomly drop out 10% of the neurons (Prevent overfitting)
    #categorical output of the angle
    angle_out = Dense(15, activation='softmax', name='angle_out')(x)        # Connect every input with every output and output 15 hidden units. Use Softmax to give percentage. 15 categories and find best one based off percentage 0.0-1.0

    #continous output of throttle
    throttle_out = Dense(1, activation='relu', name='throttle_out')(x)      # Reduce to 1 number, Positive number only

    model = Model(inputs=[img_in], outputs=[angle_out, throttle_out])
    model.compile(optimizer='adam',
                  loss={'angle_out': 'categorical_crossentropy',
                        'throttle_out': 'mean_absolute_error'},
                  loss_weights={'angle_out': 0.9, 'throttle_out': .001})

    return model


@click.command()
@click.option('--gpu', type=int, default=0)
@click.option('--model-type', type=str, default='baseline')
def visualizing_activation_heatmap(gpu, model_type):

    if model_type == 'baseline':
        model_path = '/workspace/data/pix_baseline_4'
    elif model_type == 'style_transfer':
        model_path = '/workspace/data/pix_baseline_transfer_all'
    else:
        raise ValueError('{} is not supported!'.format(model_type))

    model = default_categorical()
    model.load_weights(model_path)
    model.summary()

    img_folder_path = Path('/workspace/data/tub_05_25_centerlane/')
    num_jpgs = len(list(img_folder_path.glob('*.jpg')))

    save_folder_path = Path('/workspace/data/{}_max_heatmap_angle_out'.format(model_type))
    save_folder_path.mkdir(exist_ok=True, parents=True)

    for img_num in range(num_jpgs):
        img_fn = img_folder_path / '{}_cam-image_array_.jpg'.format(img_num)
        img = utils.load_img(img_fn, target_size=(120, 160))

        f = partial(visualize_cam, model=model, layer_idx=layer_idx, seed_input=img)
        with Pool(5) as p:
            all_heatmaps = p.map(f, list(range(15)))
        # all_heatmaps = []
        # for i in range(15):
        #     heatmap = visualize_cam(model, layer_idx=layer_idx, filter_indices=i,
        #                             seed_input=img)
        #     heatmap = np.uint8(cm.jet(heatmap)[..., :3] * 255)
        #     all_heatmaps.append(heatmap)

        all_heatmaps = np.stack(all_heatmaps, axis=0)
        max_heatmap = np.max(all_heatmaps, axis=0)
        fig = plt.figure()
        # remove axis from diagram
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)
        plt.imshow(overlay(img, max_heatmap, alpha=0.7))
        plt.savefig(str(save_folder_path / '{}.png'.format(img_num)))
        plt.close(fig)
        print(img_num)


if __name__=="__main__":
    visualizing_activation_heatmap()
