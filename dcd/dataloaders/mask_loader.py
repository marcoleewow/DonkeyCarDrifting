import requests
import numpy as np
import pandas as pd
import matplotlib.image as mpimg
from PIL import Image
from typing import Dict, List, Sequence
from pathlib import Path
from ast import literal_eval

import tensorflow.keras as keras


def maybe_download(data_path: str, csv_path: str):
    data_path = Path(data_path)
    is_folder_empty = not bool(list(data_path.rglob("*")))
    if is_folder_empty:
        data_path.mkdir(parents=True, exist_ok=True)
        df = read_csv_from(csv_path)
        train_df, valid_df, test_df = split_dataframe(df)
        download_data(train_df, data_path / 'train')
        download_data(valid_df, data_path / 'valid')
        download_data(test_df, data_path / 'test')


def read_csv_from(csv_path: str) -> pd.DataFrame:
    csv_path = Path(csv_path)
    assert csv_path.exists()
    df = pd.read_csv(csv_path)

    # remove the skipped data
    df = df[df['Label'] != "Skip"]
    return df


def split_dataframe(df: pd.DataFrame) -> List[pd.DataFrame]:
    total_idx = list(range(len(df)))
    train_idx = list(filter(lambda x: x % 5 == 0 or x % 5 == 1 or x % 5 == 2, total_idx))
    valid_idx = list(filter(lambda x: x % 5 == 3, total_idx))
    test_idx = list(filter(lambda x: x % 5 == 4, total_idx))
    train_df = df.iloc[train_idx]
    valid_df = df.iloc[valid_idx]
    test_df = df.iloc[test_idx]
    return train_df, valid_df, test_df


def download_data(df: pd.DataFrame, data_path: Path):
    data_path.mkdir(parents=True, exist_ok=True)

    for i, (img_url, masks) in enumerate(zip(df["Labeled Data"], df["Masks"])):
        download_single_file(data_path / '{}.jpg'.format(i), img_url)
        masks = literal_eval(masks)
        maybe_download_single_file(data_path / '{}_track_mask.png'.format(i), 'TrackArea', masks)
        maybe_download_single_file(data_path / '{}_left_mask.png'.format(i), 'LeftLane', masks)
        maybe_download_single_file(data_path / '{}_right_mask.png'.format(i), 'RightLane', masks)
        maybe_download_single_file(data_path / '{}_middle_mask.png'.format(i), 'MiddleLine', masks)


def maybe_download_single_file(sample_fn: Path, name: str, masks: Dict):
    try:
        url = masks[name]
        download_single_file(sample_fn, url)
    except KeyError:
        print('{} not found in masks for {}'.format(name, str(sample_fn)))
    except:
        print('something wrong for {} at url {}'.format(sample_fn, url))


def download_single_file(sample_fn: Path,  url: str):
    response = requests.get(url)
    if response.status_code == 200:
        with open(str(sample_fn), 'wb') as f:
            f.write(response.content)
    else:
        print('{} error code 200 for url: {}'.format(url))
        raise ValueError()


class TrackMaskDataloader(keras.utils.Sequence):
    def __init__(self,
                 data_path: str,
                 dataset_type: str,
                 csv_path: str = None,
                 trim_data: int = 0,
                 batch_size: int = 64,
                 shuffle: bool = True,
                 default_img_size: Sequence[int] = (120, 160)):
        assert dataset_type in ['train', 'valid', 'test']
        maybe_download(data_path, csv_path)
        self.root_data_path = Path(data_path)
        self.data_path = self.root_data_path / dataset_type
        self.dataset_type = dataset_type
        self.csv_path = csv_path
        self.default_img_size = default_img_size
        self.num_samples = len(list(self.data_path.glob('*.jpg')))
        if trim_data > 0:
            self.num_samples = trim_data
        self.indices = list(range(self.num_samples))
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        """
        Returns number of batches.
        """
        return int(np.floor(len(self.indices) / self.batch_size))

    def __getitem__(self, idx):
        indices = self.indices[idx*self.batch_size:(idx+1)*self.batch_size]
        return self._generate_data(indices)

    def _generate_data(self, indices: List[int]) -> Sequence[np.ndarray]:
        X = np.zeros(shape=(self.batch_size, *self.default_img_size, 3))
        Y_track = np.zeros(shape=(self.batch_size, *self.default_img_size))
        Y_left = np.zeros(shape=(self.batch_size, *self.default_img_size))
        Y_right = np.zeros(shape=(self.batch_size, *self.default_img_size))
        Y_middle = np.zeros(shape=(self.batch_size, *self.default_img_size))

        for i, idx in enumerate(indices):
            image_array = self.load_image_to_array('{}.jpg'.format(idx))
            track_array = self.load_mask_to_array('{}_track_mask.png'.format(idx))
            left_array = self.load_mask_to_array('{}_left_mask.png'.format(idx))
            right_array = self.load_mask_to_array('{}_right_mask.png'.format(idx))
            middle_array = self.load_mask_to_array('{}_middle_mask.png'.format(idx))
            if np.max(image_array) > 1:
                image_array = image_array / 255
            X[i] = image_array
            Y_track[i] = track_array
        #     Y_left[i] = left_array
        #     Y_right[i] = right_array
        #     Y_middle[i] =middle_array

        # # append the Ys
        # Y = np.stack([Y_track, Y_left, Y_right, Y_middle], axis=-1)
        Y = np.expand_dims(Y_track, axis=-1)
        assert Y.sum() != 0

        return X, Y

    def load_image_to_array(self, img_fn: str) -> np.ndarray:
        # array = mpimg.imread(str(self.data_path / img_fn))
        image = Image.open(str(self.data_path / img_fn))
        array = np.asarray(image)
        assert array.shape[:2] == self.default_img_size[:2]
        assert np.min(array) >= 0
        return array

    def load_mask_to_array(self, img_fn: str) -> np.ndarray:
        try:
            array = self.load_image_to_array(img_fn)
            # remove alpha channel and send to grayscale because it is not needed
            array = self.rgb2gray(array)
            array[array > 0.5] = 1
            array[array <= 0.5] = 0
            return array
        except:
            return np.zeros(shape=self.default_img_size)

    @staticmethod
    def rgb2gray(rgb):
        return np.dot(rgb[...,:3], [0.2990, 0.5870, 0.1140])

    def on_epoch_end(self):
        self.indices = list(range(self.num_samples))
        if self.shuffle:
            np.random.shuffle(self.indices)
