import pytest
import numpy as np
from copy import deepcopy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import binary_crossentropy

from dcd.models import UNet


@pytest.fixture(scope='module')
def batch_size():
    return 4


@pytest.fixture(scope='module')
def input_size():
    return (120, 160, 3)


@pytest.fixture(scope='module')
def model(input_size):
    optimizer = Adam(lr=0.01)
    return UNet(input_size=input_size, optimizer=optimizer, loss=binary_crossentropy)


@pytest.fixture(scope='module')
def inputs(batch_size, input_size):
    X = np.random.rand(*(batch_size, *input_size))
    Y = np.random.rand(*(batch_size, *input_size[:-1], 1))
    return X, Y


class TestUNet:
    def test_each_layer_has_gradient(self, inputs, model):
        y_true, y_pred = inputs
        old_weights = deepcopy(model.get_weights())
        model.train_on_batch(y_true, y_pred)

        for old, new in zip(old_weights, model.get_weights()):
            for old_elem, new_elem in zip(old.flatten(), new.flatten()):
                assert old_elem != new_elem

