import pytest
import inspect
from dcd.dataloaders import TrackMaskDataloader
from keras.utils import Sequence


@pytest.fixture(scope='class')
def track_mask_loader():
    return TrackMaskDataloader(data_path="/workspace/data/track_data/",
                               dataset_type='train',
                               csv_path="/workspace/data/labelbox.csv",
                               trim_data=10,
                               batch_size=2)

class TestTrackMaskLoader:

    def test_can_output_5_batches(self, track_mask_loader):
        samples = [sample for sample in track_mask_loader]
        assert len(samples) == 5

    def test_has_1_img_4_mask_in_each_sample(self, track_mask_loader):
        expected_shape = (track_mask_loader.batch_size,) + track_mask_loader.default_img_size
        for X, Y_mask in track_mask_loader:
            assert X.shape == expected_shape + (3,)
            assert Y_mask.shape == expected_shape + (4,)

    def test_data_in_correct_range(self, track_mask_loader):
        for X, _ in track_mask_loader:
            for elem in X.flatten():
                assert 0 <= elem <= 255

    def test_y_mask_contains_0_or_1_value(self, track_mask_loader):
        accuracy = 0.001
        for _, Y_mask in track_mask_loader:
            for elem in Y_mask.flatten():
                assert (elem - 1) < accuracy or (elem - 0) < accuracy
