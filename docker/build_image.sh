#! /bin/bash

if [[ "$OSTYPE" == "darwin"* ]]; then
    docker build ./docker -t donkeycardrifting:latest -f ./docker/Dockerfile.mac
elif [[ "$OSTYPE" == "linux-gnu" ]]; then
    docker build ./docker -t donkeycardrifting:latest -f ./docker/Dockerfile
fi
