#! /bin/bash

CONTAINER_NAME=dcd-container
NB_PORT=8899
TB_PORT=6077
docker run -ti -d --shm-size 8G \
       -v `pwd`:/workspace \
       -p ${NB_PORT}:8888 \
       -p ${TB_PORT}:6006 \
       --name ${CONTAINER_NAME} donkeycardrifting:latest

sleep 5s

docker exec -ti ${CONTAINER_NAME} pip install -e .

docker exec -ti ${CONTAINER_NAME} jupyter notebook list
