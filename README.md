# DonkeyCarDrifting

This repo aims to achieve zero/one shot learning for donkey car and finally getting donkey car to drift.

1. Mask Segmentations
We are still working on the mask segmentation for donkey car data.

# Requirements

nvidia-docker with default runtime set to nvidia

nvidia-driver >= 410 and CUDA 10.0

The container will be using python 3.5 with tensorflow 2.0.0a0 and keras 2.2.4


# Docker Installation

`git clone --recursive git@gitlab.com:marcoleewow/DonkeyCarDrifting.git`

`cd DonkeyCarDrifting`

`./docker/build_image.sh`

`./docker/run_container.sh`

Remember to download the csv from labelbox, remember to select outputing masks too.

Move the csv file to ./data/labelbox.csv
